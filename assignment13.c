
#include<stdio.h>
void evalGcd(int *c,int *d,int *g)
{
    int a=*c,b=*d;    
    for(int i=1;i<=a&&i<=b;i++)
        if(a%i==0&&b%i==0)
            *g=i;
}
int main()
{
    int a,b,gcd;
    printf("Enter the two numbers\n");
    scanf("%d %d",&a,&b);
    evalGcd(&a,&b,&gcd);
    printf("The gcd of the two numbers %d and %d is %d\n",a,b,gcd);
    return 0;
}