
#include<stdio.h>
int main()
{
    int a,b,s,d,m,r;
    int *ap=&a,*bp=&b,*sp=&s,*dp=&d,*mp=&m,*rp=&r;
    float q,*qp=&q;
    printf("Enter the two numbers \n");
    scanf("%d %d",&a,&b);
    *sp=*ap+*bp;
    *dp=*ap-*bp;
    *mp=*ap**bp;
    *qp=(float)(*ap)/(*bp);
    *rp=*ap%*bp;
    printf("The two numbers are %d and %d\n",*ap,*bp);
    printf("Addition:%d\n",*sp);
    printf("Difference:%d\n",*dp);
    printf("Multiplication:%d\n",*mp);
    printf("Division:%0.2f\n",*qp);
    printf("Remainder:%d\n",*rp);
    return 0;
}